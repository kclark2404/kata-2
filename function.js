let add = function (num1, num2) {
    let newNum = num1 + num2
    return newNum
}
console.log(add(2,4));

let multiply = function (value, times) {
    let product = 0
    for (let i = 0; i < times; i++) {
        product = add(value, product)
    }
    return product
}
console.log(multiply(6,8));

let power = function (value, times) {
    let exponent = 1
    for (let i = 0; i < times; i++) {
        exponent = multiply(value, exponent)
    }
    return exponent
}
console.log(power(3,4));

let factorial = function (num) {
    factor = num
    for (let i = num - 1; i >= 1; i--) {
        factor = multiply(factor, i)
    }
    return factor
}
console.log(factorial(6));